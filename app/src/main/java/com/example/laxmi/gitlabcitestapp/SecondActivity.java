package com.example.laxmi.gitlabcitestapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    public static String EXTRA_TAG = "Extra_Tag";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView textView = findViewById(R.id.tv1);
        if (getIntent() != null && getIntent().hasExtra(EXTRA_TAG)) {
            int i = getIntent().getIntExtra(EXTRA_TAG, 0);
            textView.setText(i + "");
        }

    }
}
